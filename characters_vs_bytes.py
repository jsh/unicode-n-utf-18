#!/usr/bin/env python3

english = "hello, world"
korean = "안녕, 세계"
print(f"English string has {len(english)} characters")
print(f"Korean string has {len(korean)} characters")
print(f"{english}\n{korean}")
