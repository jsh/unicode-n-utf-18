#!/usr/bin/env python3
import emoji
def what_is_it(unk):
        print(f"{unk} is a {type(unk)}, length {len(unk)}")
s = emoji.emojize(":thumbs_up:")  # Nice!
b = bytes(s, encoding="utf-8")    # Turn it into utf-8-encoded bytes.
what_is_it(s)                     # Print and give info
what_is_it(b)
with open("my_output_chars", "w", encoding='utf-8') as f_out:
        f_out.write(s)
with open("my_output_bytes", "wb") as f_out:
        f_out.write(b)            # Got encoded earlier
